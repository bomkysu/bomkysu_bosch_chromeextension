let extActive = true;

const ultraMode = {
	toggle: false,
	18: false,
	16: false,
	65: false
};

const defaultEventHandlersContainer = [];

const defaultHandlers = [
	'onselectstart',
	'oncopy',
	'oncontextmenu',
	'onclick',
	'onkeypress',
	'onkeyup',
	'onkeydown',
	'onmousedown',
	'onmousemove',
	'onmouseup'
];

const newStyles = [];
const maxLoadingTime = 5000;

// chrome.commands.onCommand.addListener(function (command) {
//     if (command === "save") {
//         alert("save");
//     } else if (command === "random") {
//         alert("random");
//     }
// });

chrome.runtime.sendMessage('wait');
let extentionLoadingTimeout = setTimeout(checkExtensionStatus, maxLoadingTime);

window.addEventListener('load', checkExtensionStatus);
// window.addEventListener('DOMContentLoaded', checkExtensionStatus);		// lost time waiting 

var keyEvent_notInput = function(event) 
{
	console.log("keyEvent_notInput: \n", event);	
}; 
var keyEvent = function(event) 
{
	// console.log("keyEvent: \n", event);	
	console.log("document.activeElement.nodeName: \n", document.activeElement.nodeName);	

	if ("INPUT" != document.activeElement.nodeName && "TEXTAREA" != document.activeElement.nodeName) 
	{
		keyEvent_notInput(event); 
	}
	else
	{
		document.activeElement.style.paddingLeft="2px";  // IGNORE BOLD BODER COVER THE POINTER OF TEXT BOX
	}
	// ASIGNED IN do
	// if ('r'== event.key || 'R' == event.key)
	// {
	// 	refreshCurrentCSCRMTab();
	// }
}
window.addEventListener('keydown', keyEvent, true);


function checkExtensionStatus() {
	clearTimeout(extentionLoadingTimeout);
	chrome.storage.sync.get(window.location.host, item => {
		extActive = Object.keys(item).length === 0;
		extActive ? allowSelect() : setExtensionBadgeStatus('off');
		doCitrix();
		doCSCRM();
		doOutlook();
		doExcelOnline();

		console.log("checkExtensionStatus()"); 
	});
}

function allowSelect() {
	// console.log('Loading extension - CITRIXXXXXX ');
	console.log('Loading CITRIX DESKTOP VIEWER ');

	if (newStyles.length === 0) {
		setNewStyles('user-select: text !important;', 'body', 'div', 'a', 'p', 'span');
		setNewStyles('cursor: auto; user-select: text !important;', 'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6');
		setNewStyles('background-color: #338FFF !important; color: #fff !important;', '::selection');
	}

	setNewStyleTag(newStyles);

	// window.addEventListener('keydown', ultraModeLogic, true);
	// window.addEventListener('keyup', ultraModeLogic, true);

	setExtensionBadgeStatus('ready');
	autoAllowSelectAndCopy(defaultEventHandlersContainer, window, document, document.documentElement, document.body);
}

// window.addEventListener('DOMContentLoaded', doCitrix);

function doCitrix()
{
	if (window.location.href.includes('https://abt-ism-xd.de.bosch.com/Citrix/ismXenWeb') == false)
	{
		console.log("This page not Citrix Receiver! ");
		console.log(window.location.href);
		return;
	}

	console.log(window.location);
	// waiting for load element 
	var intervalId_0 = setInterval( () => {
		console.log("waiting for loading element: username");
		if (document.getElementById("username"))
		{
			document.getElementById("username").value = "apac\\nun4hc";
			document.getElementById("password").value = localStorage.getItem("password");
			document.getElementById("loginBtn").click();	
			clearInterval(intervalId_0);
		}
	}, 500);
	
	// waiting for load element 
	var intervalId = setInterval( () => {
		console.log("waiting for loading element: storeapp-details-link");
		if (document.getElementsByClassName("storeapp-details-link")[0])
		{
			document.getElementsByClassName("storeapp-details-link")[0].click();	
			clearInterval(intervalId);
		}
	}, 500);
}

var curCSCRMDocument = {}; 

// var intervalId_refreshCRCRM; 
function doCSCRM()
{
	if ( (window.location.href == "https://abt-cscrm.de.bosch.com/cqweb/") || (window.location.href == "https://abt-cscrm.de.bosch.com/cqweb"))
	{
		keepInteractive5s();
		// console.log(document);
		curCSCRMDocument = document; 
		if (document.getElementById('toolConnectLogin').innerText.includes('Log out') == true)
		{
			console.log("CSCRM already logged in!");
			// return;
		}
		else
		{
			// input pass
			// waiting for loading element: passwordId
			var intervalId_PasswordCSCRM = setInterval( () => 
			{
				console.log("LOOP_PasswordCSCRM: " + intervalId_PasswordCSCRM, "waiting for loading element: passwordId");
				// console.log(window.document.getElementById("passwordId"));
				// console.log($("#passwordId"));
				// if (window.document.getElementById("passwordId"))
				if ($("#passwordId"))
				{
					console.log('zoooo..!!');
					// document.getElementById("username").value = "apac\\nun4hc";
					document.getElementById("passwordId").value = localStorage.getItem("password");
					// $("#passwordId").value = localStorage.getItem("password");
					// localStorage.setItem("password", "********");	// this will be cleared when PC shutdown 
					document.getElementById("loginButtonId").click();	
					// $("#loginButtonId").click();	
					clearInterval(intervalId_PasswordCSCRM);
				}
			}, 500);
			// click login ... 
			// waiting for load element 
			var intervalId_DatabaseSelection = setInterval( () => {
				console.log("LOOP_DatabaseSelection: " + intervalId_DatabaseSelection, "waiting for loading element: cqConnectSubmitButtonId_label");
				if (document.getElementById("cqConnectSubmitButtonId_label"))
				{
					console.log('zoooo..!!');
					document.getElementById("cqConnectSubmitButtonId_label").click();	
					clearInterval(intervalId_DatabaseSelection);
				}
			}, 500);
		}

		var intervalId_refreshCRCRM = setInterval(function(){
			console.log("Interval called. ID: " + intervalId_refreshCRCRM);
			if ( true == fillTheSearchTextBox() )
			{
				CSCRMSearchButtonClick();
				console.log("Seacrh button has been clicked");
			}
			else
			{
				refreshCurrentCSCRMTab();
				console.log("Refresh button has been clicked");
			}
		}, 10*60*1000 + 0*1000);

		keyEvent_notInput = function(event) 
		{
			if ('r'== event.key || 'R' == event.key)
			{
				console.log("r || R key down...");
				refreshCurrentCSCRMTab();
			}
			else
			{
				// <IGNORED> // THIS METHOD ONLY WORK WITH EXISTED TAB 
				// var arr_dijitTabInner = document.querySelectorAll(".dijitTabInner");
				// arr_dijitTabInner.forEach((item) => 
				// {
				// 	item.onclick = () =>
				// 	{
				// 		console.log("new tab is clicked");
						
				// 	}
				// })
				// <IGNORED>
			}
		}

		document.body.onclick = (ev) =>
		{
			var val_timeout = 69;
			// console.log("master click", ev);
			if (ev.srcElement.classList.contains("dijitTab") || ev.srcElement.classList.contains("tabLabel")) 
			{
				console.log("new tab is clicked");
				val_timeout = 369;
			}
			else if (ev.srcElement.classList.contains("dojoxGridCell") || ev.srcElement.classList.contains("bom-cscrm-id"))
			{
				console.log("new ID is clicked");
				val_timeout = 3000;
			}
			setTimeout(() =>
			{
				if (null == document.querySelector(".dijitVisible .dijitVisible .bom-cscrm-id"))
				{
					// check_cqFormEdit()
					// 		.then(() => 
					// 		{
					// 			console.log(document.querySelectorAll(".bom-cscrm-id"));
					// 			check_bom_cscrm_id();
					// 		});
					// THE ITEM check_cqFormEdit CAUSED Headline blank. don't know why... 20210401_1355
				}
				else
				{
				}
				check_dojoxGrid_cell();  // the dojoxGrid_cell often heal to original status, so have to check gain 
			}, val_timeout);
		}
	}

	document.querySelectorAll("#cq_widget_CqTextBox_2").onclick = () =>
	{
		console.log("__", this.value)
	}

}

function stopIntervalRefreshCSCRMTab()
{
	// CAN NOT CALLED IN CHROME CONSOLE. 
	clearInterval(intervalId_refreshCRCRM);
}

function refreshCurrentCSCRMTab() 
{	
	// if (true == window.location.href.includes("https://abt-cscrm.de.bosch.com/cqweb"))
	// {
		document.querySelector(".dijitVisible span.MenuIcon_refresh").click(); 
	// }
}

function fillTheSearchTextBox()
{
	if (document.querySelector(".dijitVisible .cqRecordName"))
	{
		// document.querySelector("#cqFindRecordString").value = document.querySelector(".dijitVisible .cqRecordName").innerText.replace("Request:", "").replace("Defect:", "").replace("PlanningTask:", "").replace("Task:", "");
		document.querySelector("#cqFindRecordString").value = 
			document.querySelector(".dijitVisible .cqRecordName").innerText.match(/CSCRM\d+/g);
		console.log("Copied the CURRENT ID to search box");
		return true;
	}
	else
	{
		console.log("Current tab don't have any CSCRM ID..");
		return false;
	}
}

function CSCRMSearchButtonClick() 
{	
	document.querySelector("#cqFindRecordButton").click(); 
}

function CSCRMTitleLOGOClick() 
{	
	document.querySelector("#cqPropTitleImg").click(); 
}


function check_dojoxGrid_cell()
{
	return new Promise(() => 
	{
		var arr_dojoxGrid_cell = document.querySelectorAll(".dijitVisible .dijitVisible .dojoxGrid-cell");
		arr_dojoxGrid_cell.forEach((item) => 
		{
			if (null != item.innerText.match(/CSCRM\d+/g))
			{
				// console.warn(item.innerText);
				item.style.fontSize = "11px";
				item.style.color="#00CC00";
				item.classList.add("bom-cscrm-id");
				item.onclick = () =>
				{
					console.log("CSCRM ID CLICKED..!");
					document.querySelector("#cqFindRecordString").value = item.innerText;
					document.querySelector("#cqFindRecordButton").click(); 
				}
			}
			else
			{
				console.log(item.innerText);
			}
		})		
	});
}

function check_cqFormEdit()
{
	return new Promise((resolve, reject) => 
	{
		var arr_cqFormEdit = document.querySelectorAll(".dijitVisible .dijitVisible .cqFormEdit div div");
		arr_cqFormEdit.forEach((item) =>
		{
			item.innerHTML = item.innerHTML.replace(/(CSCRM\d+)/g, '<a class="bom-cscrm-id"> $1</a>');
		});
		resolve(69);	
	});
}

function check_bom_cscrm_id()
{
	var arr_cscrm_id = document.querySelectorAll(".bom-cscrm-id");
	arr_cscrm_id.forEach((item) => 
	{
		if (null != item.innerText.match(/CSCRM\d+/g))
		{
			// console.warn(item.innerText);
			item.style.fontSize = "11px";
			item.style.color="#00CC00";
			item.onclick = () =>
			{
				console.log("CSCRM ID CLICKED..!");
				document.querySelector("#cqFindRecordString").value = item.innerText;
				document.querySelector("#cqFindRecordButton").click(); 
			}
			item.oncontextmenu = (_event) =>
			{
				_event.preventDefault();
				// window.prompt("Copy to clipboard: Ctrl+C, Enter", item.innerText);
				var tempstr = document.querySelector("#cqFindRecordString").value;
				document.querySelector("#cqFindRecordString").value = item.innerText;
				document.querySelector("#cqFindRecordString").select();
				document.execCommand('copy');
				document.querySelector("#cqFindRecordString").value = tempstr;
			}
		}
		else
		{
			console.log(item.innerText);
		}
	})
}

function beep() {
    // var snd = new Audio("data:audio/wav;base64,//uQRAAAAWMSLwUIYAAsYkXgoQwAEaYLWfkWgAI0wWs/ItAAAGDgYtAgAyN+QWaAAihwMWm4G8QQRDiMcCBcH3Cc+CDv/7xA4Tvh9Rz/y8QADBwMWgQAZG/ILNAARQ4GLTcDeIIIhxGOBAuD7hOfBB3/94gcJ3w+o5/5eIAIAAAVwWgQAVQ2ORaIQwEMAJiDg95G4nQL7mQVWI6GwRcfsZAcsKkJvxgxEjzFUgfHoSQ9Qq7KNwqHwuB13MA4a1q/DmBrHgPcmjiGoh//EwC5nGPEmS4RcfkVKOhJf+WOgoxJclFz3kgn//dBA+ya1GhurNn8zb//9NNutNuhz31f////9vt///z+IdAEAAAK4LQIAKobHItEIYCGAExBwe8jcToF9zIKrEdDYIuP2MgOWFSE34wYiR5iqQPj0JIeoVdlG4VD4XA67mAcNa1fhzA1jwHuTRxDUQ//iYBczjHiTJcIuPyKlHQkv/LHQUYkuSi57yQT//uggfZNajQ3Vmz+Zt//+mm3Wm3Q576v////+32///5/EOgAAADVghQAAAAA//uQZAUAB1WI0PZugAAAAAoQwAAAEk3nRd2qAAAAACiDgAAAAAAABCqEEQRLCgwpBGMlJkIz8jKhGvj4k6jzRnqasNKIeoh5gI7BJaC1A1AoNBjJgbyApVS4IDlZgDU5WUAxEKDNmmALHzZp0Fkz1FMTmGFl1FMEyodIavcCAUHDWrKAIA4aa2oCgILEBupZgHvAhEBcZ6joQBxS76AgccrFlczBvKLC0QI2cBoCFvfTDAo7eoOQInqDPBtvrDEZBNYN5xwNwxQRfw8ZQ5wQVLvO8OYU+mHvFLlDh05Mdg7BT6YrRPpCBznMB2r//xKJjyyOh+cImr2/4doscwD6neZjuZR4AgAABYAAAABy1xcdQtxYBYYZdifkUDgzzXaXn98Z0oi9ILU5mBjFANmRwlVJ3/6jYDAmxaiDG3/6xjQQCCKkRb/6kg/wW+kSJ5//rLobkLSiKmqP/0ikJuDaSaSf/6JiLYLEYnW/+kXg1WRVJL/9EmQ1YZIsv/6Qzwy5qk7/+tEU0nkls3/zIUMPKNX/6yZLf+kFgAfgGyLFAUwY//uQZAUABcd5UiNPVXAAAApAAAAAE0VZQKw9ISAAACgAAAAAVQIygIElVrFkBS+Jhi+EAuu+lKAkYUEIsmEAEoMeDmCETMvfSHTGkF5RWH7kz/ESHWPAq/kcCRhqBtMdokPdM7vil7RG98A2sc7zO6ZvTdM7pmOUAZTnJW+NXxqmd41dqJ6mLTXxrPpnV8avaIf5SvL7pndPvPpndJR9Kuu8fePvuiuhorgWjp7Mf/PRjxcFCPDkW31srioCExivv9lcwKEaHsf/7ow2Fl1T/9RkXgEhYElAoCLFtMArxwivDJJ+bR1HTKJdlEoTELCIqgEwVGSQ+hIm0NbK8WXcTEI0UPoa2NbG4y2K00JEWbZavJXkYaqo9CRHS55FcZTjKEk3NKoCYUnSQ0rWxrZbFKbKIhOKPZe1cJKzZSaQrIyULHDZmV5K4xySsDRKWOruanGtjLJXFEmwaIbDLX0hIPBUQPVFVkQkDoUNfSoDgQGKPekoxeGzA4DUvnn4bxzcZrtJyipKfPNy5w+9lnXwgqsiyHNeSVpemw4bWb9psYeq//uQZBoABQt4yMVxYAIAAAkQoAAAHvYpL5m6AAgAACXDAAAAD59jblTirQe9upFsmZbpMudy7Lz1X1DYsxOOSWpfPqNX2WqktK0DMvuGwlbNj44TleLPQ+Gsfb+GOWOKJoIrWb3cIMeeON6lz2umTqMXV8Mj30yWPpjoSa9ujK8SyeJP5y5mOW1D6hvLepeveEAEDo0mgCRClOEgANv3B9a6fikgUSu/DmAMATrGx7nng5p5iimPNZsfQLYB2sDLIkzRKZOHGAaUyDcpFBSLG9MCQALgAIgQs2YunOszLSAyQYPVC2YdGGeHD2dTdJk1pAHGAWDjnkcLKFymS3RQZTInzySoBwMG0QueC3gMsCEYxUqlrcxK6k1LQQcsmyYeQPdC2YfuGPASCBkcVMQQqpVJshui1tkXQJQV0OXGAZMXSOEEBRirXbVRQW7ugq7IM7rPWSZyDlM3IuNEkxzCOJ0ny2ThNkyRai1b6ev//3dzNGzNb//4uAvHT5sURcZCFcuKLhOFs8mLAAEAt4UWAAIABAAAAAB4qbHo0tIjVkUU//uQZAwABfSFz3ZqQAAAAAngwAAAE1HjMp2qAAAAACZDgAAAD5UkTE1UgZEUExqYynN1qZvqIOREEFmBcJQkwdxiFtw0qEOkGYfRDifBui9MQg4QAHAqWtAWHoCxu1Yf4VfWLPIM2mHDFsbQEVGwyqQoQcwnfHeIkNt9YnkiaS1oizycqJrx4KOQjahZxWbcZgztj2c49nKmkId44S71j0c8eV9yDK6uPRzx5X18eDvjvQ6yKo9ZSS6l//8elePK/Lf//IInrOF/FvDoADYAGBMGb7FtErm5MXMlmPAJQVgWta7Zx2go+8xJ0UiCb8LHHdftWyLJE0QIAIsI+UbXu67dZMjmgDGCGl1H+vpF4NSDckSIkk7Vd+sxEhBQMRU8j/12UIRhzSaUdQ+rQU5kGeFxm+hb1oh6pWWmv3uvmReDl0UnvtapVaIzo1jZbf/pD6ElLqSX+rUmOQNpJFa/r+sa4e/pBlAABoAAAAA3CUgShLdGIxsY7AUABPRrgCABdDuQ5GC7DqPQCgbbJUAoRSUj+NIEig0YfyWUho1VBBBA//uQZB4ABZx5zfMakeAAAAmwAAAAF5F3P0w9GtAAACfAAAAAwLhMDmAYWMgVEG1U0FIGCBgXBXAtfMH10000EEEEEECUBYln03TTTdNBDZopopYvrTTdNa325mImNg3TTPV9q3pmY0xoO6bv3r00y+IDGid/9aaaZTGMuj9mpu9Mpio1dXrr5HERTZSmqU36A3CumzN/9Robv/Xx4v9ijkSRSNLQhAWumap82WRSBUqXStV/YcS+XVLnSS+WLDroqArFkMEsAS+eWmrUzrO0oEmE40RlMZ5+ODIkAyKAGUwZ3mVKmcamcJnMW26MRPgUw6j+LkhyHGVGYjSUUKNpuJUQoOIAyDvEyG8S5yfK6dhZc0Tx1KI/gviKL6qvvFs1+bWtaz58uUNnryq6kt5RzOCkPWlVqVX2a/EEBUdU1KrXLf40GoiiFXK///qpoiDXrOgqDR38JB0bw7SoL+ZB9o1RCkQjQ2CBYZKd/+VJxZRRZlqSkKiws0WFxUyCwsKiMy7hUVFhIaCrNQsKkTIsLivwKKigsj8XYlwt/WKi2N4d//uQRCSAAjURNIHpMZBGYiaQPSYyAAABLAAAAAAAACWAAAAApUF/Mg+0aohSIRobBAsMlO//Kk4soosy1JSFRYWaLC4qZBYWFRGZdwqKiwkNBVmoWFSJkWFxX4FFRQWR+LsS4W/rFRb/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////VEFHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAU291bmRib3kuZGUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMjAwNGh0dHA6Ly93d3cuc291bmRib3kuZGUAAAAAAAAAACU=");  
    var snd = new Audio("data:audio/wav;base64,UklGRsYPAABXQVZFZm10IBAAAAABAAEAQB8AAIA+AAACABAAZGF0YaIPAAAAALYAbAEiAtgCjgNDBPkErwVkBhoHzweFCDoJ7wmjClgLDAzBDHUNKQ7cDpAPQxD2EKgRWxINE78TcBQhFdIVghYyF+IXkRhAGe8ZnRpLG/gbpRxRHf0dqR5UH/4fqCBSIfshoyJLI/IjmSQ/JeUliiYuJ9IndSgXKbkpWir7KpsrOizYLHYtEy6vLksv5S9/MBkxsTFJMuAydjMLNKA0MzXGNVg26TZ5Nwk4lzglObE5PTrIOlI72ztjPOo8cD31PXo+/T5/PwBAgED/QH5B+0F3QvJCbEPkQ1xE00RJRb1FMUajRhRHhEfzR2FIzkg5SaRJDUp1StxKQkumSwlMbEzMTCxNi03oTUROn074TlFPqE/9T1JQpVD3UEhRl1HlUTJSflLIUhFTWVOfU+RTJ1RqVKtU6lQpVWZVoVXcVRVWTFaCVrdW61YdV01XfVerV9dXAlgsWFVYfFihWMVY6FgKWSpZSFllWYFZm1m0WcxZ4ln3WQpaHFosWjtaSVpVWmBaaVpxWndafFqAWoJag1qCWoBafFp3WnFaaVpgWlVaSVo7WixaHFoKWvdZ4lnMWbRZm1mBWWVZSFkqWQpZ6FjFWKFYfFhVWCxYAljXV6tXfVdNVx1X61a3VoJWTFYVVtxVoVVmVSlV6lSrVGpUJ1TkU59TWVMRU8hSflIyUuVRl1FIUfdQpVBSUP1PqE9RT/hOn05ETuhNi00sTcxMbEwJTKZLQkvcSnVKDUqkSTlJzkhhSPNHhEcUR6NGMUa9RUlF00RcRORDbEPyQndC+0F+Qf9AgEAAQH8//T56PvU9cD3qPGM82ztSO8g6PTqxOSU5lzgJOHk36TZYNsY1MzWgNAs0djPgMkkysTEZMX8w5S9LL68uEy52LdgsOiybK/sqWiq5KRcpdSjSJy4niiblJT8lmSTyI0sjoyL7IVIhqCD+H1QfqR79HVEdpRz4G0sbnRrvGUAZkRjiFzIXghbSFSEVcBS/Ew0TWxKoEfYQQxCQD9wOKQ51DcEMDAxYC6MK7wk6CYUIzwcaB2QGrwX5BEMEjgPYAiICbAG2AAAASv+U/t79KP1y/L37B/tR+pz55vgx+Hv3xvYR9l31qPT08z/zi/LX8STxcPC97wrvWO6l7fPsQeyQ69/qLup+6c7oHuhv58DmEeZj5bXkCORb46/iA+JX4azgAuBY367eBd5d3bXcDtxn28HaG9p22dLYLtiL1+nWR9am1QXVZdTG0yjTitLt0VHRtdAb0IHP585PzrfNIM2KzPXLYMvNyjrKqMkXyYfI98dpx9vGT8bDxTjFrsQlxJ3DFsOQwgvChsEDwYHAAMCAvwG/gr4Fvom9Dr2UvBy8pLstu7e6Q7rPuV257Lh8uA24n7cyt8e2XLbztYu1JLW+tFq097OUszSz1LJ1shiyvLFhsQixr7BYsAOwrq9brwmvuK5prhuuzq2CrTit76ynrGGsHKzZq5arVasWq9eqmqpfqiSq66m0qX6pSakVqeOos6iDqFWoKaj+p9Snq6eEp1+nO6cYp/am1qa4ppumf6ZlpkymNKYepgmm9qXkpdSlxaW3pauloKWXpY+liaWEpYClfqV9pX6lgKWEpYmlj6WXpaClq6W3pcWl1KXkpfalCaYepjSmTKZlpn+mm6a4ptam9qYYpzunX6eEp6un1Kf+pymoVaiDqLOo46gVqUmpfqm0qeupJKpfqpqq16oWq1WrlqvZqxysYaynrO+sOK2Crc6tG65prriuCa9br66vA7BYsK+wCLFhsbyxGLJ1stSyNLOUs/ezWrS+tCS1i7XztVy2x7Yyt5+3Dbh8uOy4XbnPuUO6t7otu6S7HLyUvA69ib0FvoK+Ab+AvwDAgcADwYbBC8KQwhbDncMlxK7EOMXDxU/G28Zpx/fHh8gXyajJOsrNymDL9cuKzCDNt81PzufOgc8b0LXQUdHt0YrSKNPG02XUBdWm1UfW6daL1y7Y0th22Rvawdpn2w7ctdxd3QXert5Y3wLgrOBX4QPir+Jb4wjkteRj5RHmwOZv5x7ozuh+6S7q3+qQ60Hs8+yl7VjuCu+973DwJPHX8YvyP/P086j0XfUR9sb2e/cx+Ob4nPlR+gf7vfty/Cj93v2U/kr/AAC2AGwBIgLYAo4DQwT5BK8FZAYaB88HhQg6Ce8JowpYCwwMwQx1DSkO3A6QD0MQ9hCoEVsSDRO/E3AUIRXSFYIWMhfiF5EYQBnvGZ0aSxv4G6UcUR39HakeVB/+H6ggUiH7IaMiSyPyI5kkPyXlJYomLifSJ3UoFym5KVoq+yqbKzos2Cx2LRMury5LL+UvfzAZMbExSTLgMnYzCzSgNDM1xjVYNuk2eTcJOJc4JTmxOT06yDpSO9s7YzzqPHA99T16Pv0+fz8AQIBA/0B+QftBd0LyQmxD5ENcRNNESUW9RTFGo0YUR4RH80dhSM5IOUmkSQ1KdUrcSkJLpksJTGxMzEwsTYtN6E1ETp9O+E5RT6hP/U9SUKVQ91BIUZdR5VEyUn5SyFIRU1lTn1PkUydUalSrVOpUKVVmVaFV3FUVVkxWgla3VutWHVdNV31Xq1fXVwJYLFhVWHxYoVjFWOhYClkqWUhZZVmBWZtZtFnMWeJZ91kKWhxaLFo7WklaVVpgWmlacVp3WnxagFqCWoNaglqAWnxad1pxWmlaYFpVWklaO1osWhxaClr3WeJZzFm0WZtZgVllWUhZKlkKWehYxVihWHxYVVgsWAJY11erV31XTVcdV+tWt1aCVkxWFVbcVaFVZlUpVepUq1RqVCdU5FOfU1lTEVPIUn5SMlLlUZdRSFH3UKVQUlD9T6hPUU/4Tp9ORE7oTYtNLE3MTGxMCUymS0JL3Ep1Sg1KpEk5Sc5IYUjzR4RHFEejRjFGvUVJRdNEXETkQ2xD8kJ3QvtBfkH/QIBAAEB/P/0+ej71PXA96jxjPNs7UjvIOj06sTklOZc4CTh5N+k2WDbGNTM1oDQLNHYz4DJJMrExGTF/MOUvSy+vLhMudi3YLDosmyv7KloquSkXKXUo0icuJ4om5SU/JZkk8iNLI6Mi+yFSIagg/h9UH6ke/R1RHaUc+BtLG50a7xlAGZEY4hcyF4IW0hUhFXAUvxMNE1sSqBH2EEMQkA/cDikOdQ3BDAwMWAujCu8JOgmFCM8HGgdkBq8F+QRDBI4D2AIiAmwBtgAAAEr/lP7e/Sj9cvy9+wf7Ufqc+eb4Mfh798b2EfZd9aj09PM/84vy1/Ek8XDwve8K71jupe3z7EHskOvf6i7qfunO6B7ob+fA5hHmY+W15AjkW+Ov4gPiV+Gs4ALgWN+u3gXeXd213A7cZ9vB2hvadtnS2C7Yi9fp1kfWptUF1WXUxtMo04rS7dFR0bXQG9CBz+fOT863zSDNisz1y2DLzco6yqjJF8mHyPfHacfbxk/Gw8U4xa7EJcSdwxbDkMILwobBA8GBwADAgL8Bv4K+Bb6JvQ69lLwcvKS7Lbu3ukO6z7lduey4fLgNuJ+3MrfHtly287WLtSS1vrRatPezlLM0s9SydbIYsryxYbEIsa+wWLADsK6vW68Jr7iuaa4brs6tgq04re+sp6xhrBys2auWq1WrFqvXqpqqX6okquuptKl+qUmpFanjqLOog6hVqCmo/qfUp6unhKdfpzunGKf2ptamuKabpn+mZaZMpjSmHqYJpval5KXUpcWlt6WrpaCll6WPpYmlhKWApX6lfaV+pYClhKWJpY+ll6Wgpault6XFpdSl5KX2pQmmHqY0pkymZaZ/ppumuKbWpvamGKc7p1+nhKerp9Sn/qcpqFWog6izqOOoFalJqX6ptKnrqSSqX6qaqteqFqtVq5ar2ascrGGsp6zvrDitgq3OrRuuaa64rgmvW6+urwOwWLCvsAixYbG8sRiydbLUsjSzlLP3s1q0vrQktYu187Vctse2Mreftw24fLjsuF25z7lDure6Lbukuxy8lLwOvYm9Bb6CvgG/gL8AwIHAA8GGwQvCkMIWw53DJcSuxDjFw8VPxtvGacf3x4fIF8moyTrKzcpgy/XLiswgzbfNT87nzoHPG9C10FHR7dGK0ijTxtNl1AXVptVH1unWi9cu2NLYdtkb2sHaZ9sO3LXcXd0F3q7eWN8C4KzgV+ED4q/iW+MI5LXkY+UR5sDmb+ce6M7ofuku6t/qkOtB7PPspe1Y7grvve9w8CTx1/GL8j/z9POo9F31EfbG9nv3Mfjm+Jz5UfoH+737cvwo/d79lP5K/wAAtgBsASIC2AKOA0ME+QSvBWQGGgfPB4UIOgnvCaMKWAsMDMEMdQ0pDtwOkA9DEPYQqBFbEg0TvxNwFCEV0hWCFjIX4heRGEAZ7xmdGksb+BulHFEd/R2pHlQf/h+oIFIh+yGjIksj8iOZJD8l5SWKJi4n0id1KBcpuSlaKvsqmys6LNgsdi0TLq8uSy/lL38wGTGxMUky4DJ2Mws0oDQzNcY1WDbpNnk3CTiXOCU5sTk9Osg6UjvbO2M86jxwPfU9ej79Pn8/AECAQP9AfkH7QXdC8kJsQ+RDXETTRElFvUUxRqNGFEeER/NHYUjOSDlJpEkNSnVK3EpCS6ZLCUxsTMxMLE2LTehNRE6fTvhOUU+oT/1PUlClUPdQSFGXUeVRMlJ+UshSEVNZU59T5FMnVGpUq1TqVClVZlWhVdxVFVZMVoJWt1brVh1XTVd9V6tX11cCWCxYVVh8WKFYxVjoWApZKllIWWVZgVmbWbRZzFniWfdZClocWixaO1pJWlVaYFppWnFad1p8WoBaglqDWoJagFp8WndacVppWmBaVVpJWjtaLFocWgpa91niWcxZtFmbWYFZZVlIWSpZClnoWMVYoVh8WFVYLFgCWNdXq1d9V01XHVfrVrdWglZMVhVW3FWhVWZVKVXqVKtUalQnVORTn1NZUxFTyFJ+UjJS5VGXUUhR91ClUFJQ/U+oT1FP+E6fTkRO6E2LTSxNzExsTAlMpktCS9xKdUoNSqRJOUnOSGFI80eERxRHo0YxRr1FSUXTRFxE5ENsQ/JCd0L7QX5B/0CAQABAfz/9Pno+9T1wPeo8YzzbO1I7yDo9OrE5JTmXOAk4eTfpNlg2xjUzNfczvjKIMVUwJC/3Lc0spiuCKmEpRCgqJxMm/yTvI+Ii2SHTINAf0R7WHd4c6hv6Gg0aJBk/GF0XgBamFdAU/hMwE2USnxHdEB8QZA+uDvwNTg2kDP8LXQvACiYKkgkBCXUI7AdpB+kGbgb3BYUFFwWtBEgE5wOKAzID3wKPAkUC/wG9AYABRwETAeMAuACRAG8AUgA5ACQAFAAJAAIAAAA=");
    // snd.volume = 0.01;  // can be heard by headphone // can keep the tab active for about 4hrs //
    // snd.volume = 0.001;  // can not be heard // can not keep the tab active //
    snd.volume = 0.1;
    snd.play();
}

function keepInteractive5s()
{
	var intervalId_refreshCRCRM = setInterval(() => {
		// console.log("BEEP is called. ID: " + intervalId_refreshCRCRM);
		beep();		
	}, 5000);
}





function doOutlook()	// not work 
{
	// https://rb-owa.apac.bosch.com/owa/auth/logon.aspx?replaceCurrent=1&reason=2&url=https%3a%2f%2frb-owa.apac.bosch.com%2fowa%2f%23authRedirect%3dtrue
	if (window.location.href.includes('https://rb-owa.apac.bosch.com/owa/auth/logon.aspx?replaceCurrent') )
	{
		var intervalId_0 = setInterval( () => 
		{
			if (document.getElementById("password"))
			{
				document.getElementById("username").value = "apac\\nun4hc";
				document.getElementById("password").value = localStorage.getItem("password");
				console.log(document.getElementById("password").value);
				// document.getElementById("loginBtn").click();	
				// clkLgn();	// script.js:232 Uncaught ReferenceError: clkLgn is not defined
				document.getElementsByClassName('signinbutton')[0].click();
				clearInterval(intervalId_0);
			}
		}, 500);

	}

}

var curExcelOnlineDocument = {}; 

function doExcelOnline()
{
	if (window.location.href.includes("https://sites.inside-share2.bosch.com/sites/") )
	{
		console.log("This is SHARE POINT.. ");
		curExcelOnlineDocument = document; 
		// var intervalId_ExcelOnline = setInterval( () => 
		// {
		// 	console.log("Waiting for 'action' ");
		// 	if (curExcelOnlineDocument.getElementById("action"))
		// 	{
		// 		curExcelOnlineDocument.getElementById("action").click();
		// 		clearInterval(intervalId_ExcelOnline);
		// 	}
		// }, 500);

	}
}

/* *** APPEND DOM *** */
function setNewStyles(style, ...selectors) {
	const resultCss = `${selectors.join(', ')}{ ${style} }`;
	newStyles.push(resultCss);
}
function setNewStyleTag(stylesArray) {
	const newStyleTag = document.createElement('style');
	newStyleTag.type = 'text/css';
	for (let i = 0; i < stylesArray.length; i++) {
		newStyleTag.appendChild(document.createTextNode(stylesArray[i]));
	}
	newStyleTag.setAttribute('data-asas-style', '');
	document.head.appendChild(newStyleTag);
	// appendIFrame('head', newStyleTag);
}
function appendIFrame(target, obj) {
	const iframes = window.frames;
	for (let i = 0; i < iframes.length; i++) {
		try {
			iframes[i].document[target].appendChild(obj);
			// console.log('Appended Iframe');
		} catch (err) {
			// console.log(err);
		}
	}
}
/* *** APPEND DOM END *** */

/* *** ULTRA MODE LOGIC *** */
// const ultraModeLogic = function(event) {
// 	ultraKeyPressed(event);
// 	ultraCombinationPressed();
// };
// function ultraKeyPressed(event) {
// 	if (event.type == 'keydown') {
// 		if (ultraMode.hasOwnProperty(event.keyCode)) {
// 			ultraMode[event.keyCode] = true;
// 		}
// 	} else if (event.type == 'keyup') {
// 		if (ultraMode.hasOwnProperty(event.keyCode)) {
// 			ultraMode[event.keyCode] = false;
// 		}
// 	}
// }
// function ultraCombinationPressed() {
// 	if (ultraMode[18] && ultraMode[16] && ultraMode[65]) {
// 		ultraMode.toggle = !ultraMode.toggle;
// 		console.log('ultra', ultraMode.toggle);
// 		toggleUltraHandlers('selectstart mousedown contextmenu copy keydown', ultraPropagation, ultraMode.toggle);
// 	}
// }
// function toggleUltraHandlers(events, callback, activate) {
// 	events = events.split(' ');
// 	if (activate) {
// 		events.forEach(function(item) {
// 			window.addEventListener(item, callback, true);
// 		});
// 		setExtensionBadgeStatus('ultra');
// 	} else {
// 		events.forEach(function(item) {
// 			window.removeEventListener(item, callback, true);
// 		});
// 		setExtensionBadgeStatus('ready');
// 	}
// }
// const ultraPropagation = function(event) {
// 	if (ultraMode.toggle) event.stopPropagation();
// };

/* *** ULTRA MODE LOGIC END *** */

// Saving default handlers to backup them if user disable extension in POPUP
function autoAllowSelectAndCopy(arr, ...elems) {
	elems.forEach((elem, index) => {
		const elemContainer = {};
		elemContainer.refElem = elem;
		defaultHandlers.forEach(function(item) {
			elemContainer[item] = elem[item];
			elem[item] = null;
		});
		arr.push(elemContainer);
	});
}

// function disableExtension() {
// 	disableSiteHandlers(defaultEventHandlersContainer);

// 	const styleTag = document.querySelector('[data-asas-style]');
// 	if (styleTag) styleTag.remove();

// 	function disableSiteHandlers(arr) {
// 		arr.forEach(item => {
// 			for (const prop in item) {
// 				if (item[prop] === item.refElem) continue;
// 				item.refElem[prop] = item[prop];
// 			}
// 		});
// 	}

// 	// window.removeEventListener('keyup', ultraModeLogic, true);
// 	// window.removeEventListener('keydown', ultraModeLogic, true);

// 	setExtensionBadgeStatus('off');
// 	console.log('Extension disabled');
// }

function setExtensionBadgeStatus(status) {
	chrome.runtime.sendMessage(status);
}

//Manage extension from a popup settings
chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
	if (request.hasOwnProperty('extStatus')) {
		// request.extStatus ? allowSelect() : disableExtension();
	}
	if (request.hasOwnProperty('extReload')) {
		if (request.extReload) {
			console.log('Reloading...?');
			// disableExtension();
			// checkExtensionStatus();
		}
	}

	if (request.hasOwnProperty('btnOpen'))
	{
		console.log('btnOpen clicked..!!!!!__ '); 
		location="https://abt-ism-xd.de.bosch.com/Citrix/ismXenWeb/";
	}
});

function getExtensionStatus(target, callback) {
	chrome.storage.sync.get(target, items => {
		callback(chrome.runtime.lastError ? null : items);
	});
}


