let extActive = true;

const ultraMode = {
	toggle: false,
	18: false,
	16: false,
	65: false
};

const defaultEventHandlersContainer = [];

const defaultHandlers = [
	'onselectstart',
	'oncopy',
	'oncontextmenu',
	'onclick',
	'onkeypress',
	'onkeyup',
	'onkeydown',
	'onmousedown',
	'onmousemove',
	'onmouseup'
];

const newStyles = [];
const maxLoadingTime = 5000;

chrome.runtime.sendMessage('wait');
let extentionLoadingTimeout = setTimeout(checkExtensionStatus, maxLoadingTime);

window.addEventListener('load', checkExtensionStatus);
// window.addEventListener('DOMContentLoaded', checkExtensionStatus);		// lost time waiting 

function checkExtensionStatus() {
	clearTimeout(extentionLoadingTimeout);
	chrome.storage.sync.get(window.location.host, item => {
		extActive = Object.keys(item).length === 0;
		extActive ? allowSelect() : setExtensionBadgeStatus('off');
		doCitrix();
		doCSCRM();
		doOutlook();

	});
}

function allowSelect() {
	// console.log('Loading extension - CITRIXXXXXX ');
	console.log('Loading CITRIX DESKTOP VIEWER ');

	if (newStyles.length === 0) {
		setNewStyles('user-select: text !important;', 'body', 'div', 'a', 'p', 'span');
		setNewStyles('cursor: auto; user-select: text !important;', 'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6');
		setNewStyles('background-color: #338FFF !important; color: #fff !important;', '::selection');
	}

	setNewStyleTag(newStyles);

	window.addEventListener('keydown', ultraModeLogic, true);
	window.addEventListener('keyup', ultraModeLogic, true);

	setExtensionBadgeStatus('ready');
	autoAllowSelectAndCopy(defaultEventHandlersContainer, window, document, document.documentElement, document.body);
}

// window.addEventListener('DOMContentLoaded', doCitrix);

function doCitrix()
{
	if (window.location.href.includes('https://abt-ism-xd.de.bosch.com/Citrix/ismXenWeb') == false)
	{
		console.log("This page not Citrix Receiver! ");
		console.log(window.location.href);
		return;
	}

	console.log(window.location);
	// waiting for load element 
	var intervalId_0 = setInterval( () => {
		console.log("waiting for loading element: username");
		if (document.getElementById("username"))
		{
			document.getElementById("username").value = "apac\\nun4hc";
			document.getElementById("password").value = localStorage.getItem("password");
			document.getElementById("loginBtn").click();	
			clearInterval(intervalId_0);
		}
	}, 500);
	
	// waiting for load element 
	var intervalId = setInterval( () => {
		console.log("waiting for loading element: storeapp-details-link");
		if (document.getElementsByClassName("storeapp-details-link")[0])
		{
			document.getElementsByClassName("storeapp-details-link")[0].click();	
			clearInterval(intervalId);
		}
	}, 500);
}

var curCSCRMDocument = {}; 

function doCSCRM()
{
	// if ( (window.location.href.includes('CSCRM') == false) && ((window.location.href.includes('cscrm') == false)))

	// Welcome2CSCRM JUST A MINI WEB IN IFRAME 
	// "document" in this time just a child of outer document  
	// if (window.location.href.includes('Welcome2CSCRM.htm') == false)
	// {
	// 	console.log("This page not Welcome2CSCRM.htm! ");
	// 	// console.log(window.location.href);
	// 	// return;
	// }
	// else 
	
	if ( (window.location.href == "https://abt-cscrm.de.bosch.com/cqweb/") || (window.location.href == "https://abt-cscrm.de.bosch.com/cqweb"))
	{
		// console.log(document);
		curCSCRMDocument = document; 
		if (document.getElementById('toolConnectLogin').innerText.includes('Log out') == true)
		{
			console.log("CSCRM already logged in!");
			// return;
		}
		else
		{
			// input pass
			// waiting for loading element: passwordId
			var intervalId_PasswordCSCRM = setInterval( () => 
			{
				console.log("LOOP_PasswordCSCRM: " + intervalId_PasswordCSCRM, "waiting for loading element: passwordId");
				// console.log(window.document.getElementById("passwordId"));
				// console.log($("#passwordId"));
				// if (window.document.getElementById("passwordId"))
				if ($("#passwordId"))
				{
					console.log('zoooo..!!');
					// document.getElementById("username").value = "apac\\nun4hc";
					document.getElementById("passwordId").value = localStorage.getItem("password");
					// $("#passwordId").value = localStorage.getItem("password");
					// localStorage.setItem("password", "********");	// this will be cleared when PC shutdown 
					document.getElementById("loginButtonId").click();	
					// $("#loginButtonId").click();	
					clearInterval(intervalId_PasswordCSCRM);
				}
			}, 500);
			// click login ... 
			// waiting for load element 
			var intervalId_DatabaseSelection = setInterval( () => {
				console.log("LOOP_DatabaseSelection: " + intervalId_DatabaseSelection, "waiting for loading element: cqConnectSubmitButtonId_label");
				if (document.getElementById("cqConnectSubmitButtonId_label"))
				{
					console.log('zoooo..!!');
					document.getElementById("cqConnectSubmitButtonId_label").click();	
					clearInterval(intervalId_DatabaseSelection);
				}
			}, 500);
		}

		var intervalId_refresh = setInterval(function(){
			// document.getElementsByClassName("MenuIcon_refresh")[0].click();
			curCSCRMDocument.getElementsByClassName("MenuIcon_refresh")[0].click();
			console.log("Refresh Clicked");
		}, 9*60*1000 + 0*1000);

		// var intervalId_refreshButtonCheck = setInterval(function(){
		// 	// document.getElementsByClassName("MenuIcon_refresh")[0].click();
		// 	// curCSCRMDocument.getElementsByClassName("MenuIcon_refresh")[0].click();
		// 	console.log("Refresh button CHECK: ", curCSCRMDocument.getElementsByClassName("MenuIcon_refresh")[0]);
		// 	if ( ! curCSCRMDocument.getElementsByClassName("MenuIcon_refresh")[0] )
		// 	{
		// 		curCSCRMDocument.getElementById("cq_widget_CqTreeNode_9").click();
		// 	}
		// }, 0*60*1000 + 1*1000);
	
	}


	// waiting for load element 
	
	// console.log(window);
	// console.log(window.document);
	// console.log(document);
	// console.log(document.getElementById("passwordId"));
	// console.log($(document));

	// var timeoutId_PasswordCSCRM = setTimeout( () => 
	// {
	// 	console.log(window);
	// 	console.log(window.document);
	// 	console.log(document);
	// 	console.log(document.getElementById("passwordId"));
	// }, 5000);

	// var intervalId_PasswordCSCRM = setInterval( () => 
	// {
	// 	console.log("LOOP_PasswordCSCRM: " /*+ intervalId_PasswordCSCRM*/, "waiting for loading element: passwordId");
	// 	// console.log(window.document.getElementById("passwordId"));
	// 	// console.log($("#passwordId"));
	// 	// if (window.document.getElementById("passwordId"))
	// 	if ($("#passwordId"))
	// 	{
	// 		console.log('zoooo..!!');
	// 		// document.getElementById("username").value = "apac\\nun4hc";
	// 		// document.getElementById("passwordId").value = localStorage.getItem("password");
	// 		$("#passwordId").value = localStorage.getItem("password");
	// 		// localStorage.setItem("password", "********");	// this will be cleared when PC shutdown 
	// 		// document.getElementById("loginButtonId").click();	
	// 		$("#loginButtonId").click();	
	// 		// clearInterval(intervalId_PasswordCSCRM);
	// 	}
	// }, 500);
	// var timeoutId_PasswordCSCRM = setTimeout( () => 
	// {
	// 	console.log("TimeOut_PasswordCSCRM: " + timeoutId_PasswordCSCRM, "waiting for loading element: passwordId");
	// 	if (document.getElementById("passwordId"))
	// 	{
	// 		console.log('zoooo..!!');
	// 		document.getElementById("passwordId").value = localStorage.getItem("password");
	// 		document.getElementById("loginButtonId").click();	
	// 	}
	// }, 10000);




	// waiting for load element 
	// var intervalId_DatabaseSelection = setInterval( () => {
	// 	console.log("LOOP_DatabaseSelection: " + intervalId_DatabaseSelection, "waiting for loading element: cqConnectSubmitButtonId_label");
	// 	if (document.getElementById("cqConnectSubmitButtonId_label"))
	// 	{
	// 		console.log('zoooo..!!');
	// 		document.getElementById("cqConnectSubmitButtonId_label").click();	
	// 		clearInterval(intervalId_DatabaseSelection);
	// 	}
	// }, 500);

	// click reload per 9 min 
		



}

function doOutlook()	// not work 
{
	// https://rb-owa.apac.bosch.com/owa/auth/logon.aspx?replaceCurrent=1&reason=2&url=https%3a%2f%2frb-owa.apac.bosch.com%2fowa%2f%23authRedirect%3dtrue
	if (window.location.href.includes('https://rb-owa.apac.bosch.com/owa/auth/logon.aspx?replaceCurrent') )
	{
		var intervalId_0 = setInterval( () => 
		{
			if (document.getElementById("password"))
			{
				document.getElementById("username").value = "apac\\nun4hc";
				document.getElementById("password").value = localStorage.getItem("password");
				console.log(document.getElementById("password").value);
				// document.getElementById("loginBtn").click();	
				// clkLgn();	// script.js:232 Uncaught ReferenceError: clkLgn is not defined
				document.getElementsByClassName('signinbutton')[0].click();
				clearInterval(intervalId_0);
			}
		}, 500);

	}

}


/* *** APPEND DOM *** */
function setNewStyles(style, ...selectors) {
	const resultCss = `${selectors.join(', ')}{ ${style} }`;
	newStyles.push(resultCss);
}
function setNewStyleTag(stylesArray) {
	const newStyleTag = document.createElement('style');
	newStyleTag.type = 'text/css';
	for (let i = 0; i < stylesArray.length; i++) {
		newStyleTag.appendChild(document.createTextNode(stylesArray[i]));
	}
	newStyleTag.setAttribute('data-asas-style', '');
	document.head.appendChild(newStyleTag);
	// appendIFrame('head', newStyleTag);
}
function appendIFrame(target, obj) {
	const iframes = window.frames;
	for (let i = 0; i < iframes.length; i++) {
		try {
			iframes[i].document[target].appendChild(obj);
			// console.log('Appended Iframe');
		} catch (err) {
			// console.log(err);
		}
	}
}
/* *** APPEND DOM END *** */

/* *** ULTRA MODE LOGIC *** */
const ultraModeLogic = function(event) {
	ultraKeyPressed(event);
	ultraCombinationPressed();
};
function ultraKeyPressed(event) {
	if (event.type == 'keydown') {
		if (ultraMode.hasOwnProperty(event.keyCode)) {
			ultraMode[event.keyCode] = true;
		}
	} else if (event.type == 'keyup') {
		if (ultraMode.hasOwnProperty(event.keyCode)) {
			ultraMode[event.keyCode] = false;
		}
	}
}
function ultraCombinationPressed() {
	if (ultraMode[18] && ultraMode[16] && ultraMode[65]) {
		ultraMode.toggle = !ultraMode.toggle;
		console.log('ultra', ultraMode.toggle);
		toggleUltraHandlers('selectstart mousedown contextmenu copy keydown', ultraPropagation, ultraMode.toggle);
	}
}
function toggleUltraHandlers(events, callback, activate) {
	events = events.split(' ');
	if (activate) {
		events.forEach(function(item) {
			window.addEventListener(item, callback, true);
		});
		setExtensionBadgeStatus('ultra');
	} else {
		events.forEach(function(item) {
			window.removeEventListener(item, callback, true);
		});
		setExtensionBadgeStatus('ready');
	}
}
const ultraPropagation = function(event) {
	if (ultraMode.toggle) event.stopPropagation();
};

/* *** ULTRA MODE LOGIC END *** */

// Saving default handlers to backup them if user disable extension in POPUP
function autoAllowSelectAndCopy(arr, ...elems) {
	elems.forEach((elem, index) => {
		const elemContainer = {};
		elemContainer.refElem = elem;
		defaultHandlers.forEach(function(item) {
			elemContainer[item] = elem[item];
			elem[item] = null;
		});
		arr.push(elemContainer);
	});
}

function disableExtension() {
	disableSiteHandlers(defaultEventHandlersContainer);

	const styleTag = document.querySelector('[data-asas-style]');
	if (styleTag) styleTag.remove();

	function disableSiteHandlers(arr) {
		arr.forEach(item => {
			for (const prop in item) {
				if (item[prop] === item.refElem) continue;
				item.refElem[prop] = item[prop];
			}
		});
	}

	window.removeEventListener('keyup', ultraModeLogic, true);
	window.removeEventListener('keydown', ultraModeLogic, true);

	setExtensionBadgeStatus('off');
	console.log('Extension disabled');
}

function setExtensionBadgeStatus(status) {
	chrome.runtime.sendMessage(status);
}

//Manage extension from a popup settings
chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
	if (request.hasOwnProperty('extStatus')) {
		// request.extStatus ? allowSelect() : disableExtension();
	}
	if (request.hasOwnProperty('extReload')) {
		if (request.extReload) {
			console.log('Reloading...?');
			// disableExtension();
			// checkExtensionStatus();
		}
	}

	if (request.hasOwnProperty('btnOpen'))
	{
		console.log('btnOpen clicked..!!!!!__ '); 
		location="https://abt-ism-xd.de.bosch.com/Citrix/ismXenWeb/";
	}
});

function getExtensionStatus(target, callback) {
	chrome.storage.sync.get(target, items => {
		callback(chrome.runtime.lastError ? null : items);
	});
}


